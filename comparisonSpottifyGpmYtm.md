comparisonSpotifyGpmYtm
==========================


```
| **feature**                            | **spotify**                                | **ytMusic**                                          | **gpMusic**                |
| still supported                        | yes                                        | yes                                                  | no                         |
| cost/month                             | 84.50                                      | 99                                                   | 99                         |
| native linux client                    | yes                                        | no                                                   | no                         |
| community linux client                 | depends on spotifyd or spotify desktop     | yes                                                  | yes                        |
| terminal UI                            | depends on spotifyd or spotify desktop     | no                                                   | mopidy/mpd worked ok       |
| terminal command interface             | depends on spotifyd or spotify desktop     | not that ive found other than generic music controll |                            |
| tizonia compatability*                 | yes, seems not to crash now using the snap | no                                                   | yes                        |
| RPI                                    | should work, not official                  | should work, not official                            | -----                      |
| mobile                                 | yes                                        | yes                                                  | yes                        |
| webb client                            | yes                                        | yes                                                  | yes                        |
| podcasts                               | yes builtin                                | external, google podcasts                            | external,google podcasts   |
| darkmode ui                            | yes,default                                | yes                                                  | no                         |
| plays (music)videos                    | no                                         | yes native                                           | links to youtube player    |
| lyrics                                 | no                                         | yes , untested not all artists                       | i think so                 |
| playlist management (mobile) out of 64 | 32                                         | 40                                                   | 63 really good, menu based |
| playlist management webb/ui            | 55                                         | 42                                                   | 63                         |
| plays local files                      | yes untested, desktop only                 | no                                                   | yes mobile and desktop     |
| comments                               |                                            |                                                      |                            |

```






## tizonia
tizonia is a console based musicplayer(linux+) for streaming services that (used to) work really well
I have written specialized scripts to interact with it so it is fairly important if a service is supported by it

@projects